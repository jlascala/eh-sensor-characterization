import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import matplotlib.dates as mdates

import queue
import threading
import time

def main(args):
    print(args.path)
    
    data = pd.read_pickle(args.path)
    
    if args.powerOverTime:
        generatePowerOverTimeGraph(data,args)
    elif args.computeParameters:
        find_power_parameters2(data, args)
    elif args.envGraph:
        generateEnvGraph(data,args)
    elif args.luxPowerGraph:
        generateLuxPowerGraph(data,args)
        
def generatePowerOverTimeGraph(data,args=None):
    fig = plt.figure(figsize=(8.3,4.15))
    
    ax1 = fig.add_subplot(1,1,1)
    
    #ax1.plot(data.index, 1000*data.power, label=str("Power"))
    if args.fine:
        ax1.plot(data.index,1000*data.power,label="Power")
    else:
        ax1.plot(data.index,1000*data.power.rolling(window=pd.tseries.offsets.Minute()).mean(),label="Power")
        
    #ax1.legend(bbox_to_anchor=(0.8, 0.3), loc='upper left', borderaxespad=0.)
        
    #ax1.set_title("Power over Time")
    ax1.set_ylabel("Power [mW]")
    ax1.set_xlabel("Time")
    
    #ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%M:%S'))
    
    p = Path(args.path).resolve()
    fig.savefig(str(p.parent / (p.stem+"_power.eps")))
    
    fig.show()
    
def generateEnvGraph(data,args=None):
    fig = plt.figure(figsize=(8.3,6.5))
    
    temp = data.TempMeter.where(data.TempMeter>0).dropna()
    
    ax1 = fig.add_subplot(2,1,1)
    
    ax1.plot(temp.index,temp.values,"g,",label="Temperature")

    ax1.set_ylabel("Temperature [$^\circ C$]")
    ax1.set_xlabel("Time of day")
    
    lux = data.LightMeter.where(data.LightMeter>0).dropna()
    
    ax2 = fig.add_subplot(2,1,2)
    
    ax2.plot(lux.index,lux.values,"r",label="Light level")

    ax2.set_ylabel("Light level [$lux$]")
    ax2.set_xlabel("Time of day")
    
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax2.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    
    p = Path(args.path).resolve()
    fig.savefig(str(p.parent / (p.stem+"_env.eps")))
    
    fig.show()
    
def generateLuxPowerGraph(data,args=None):
    fig = plt.figure(figsize=(6.5,6.5))
    
    data = data.where(data.LightMeter>0).dropna()
    
    ax1 = fig.add_subplot(1,1,1)
    
    ax1.plot(data.LightMeter.values,(1000*data.power).values,"g,")

    ax1.set_xlabel("Light level [$lux$]")
    ax1.set_ylabel("Power [mW]")
    
    p = Path(args.path).resolve()
    fig.savefig(str(p.parent / (p.stem+"_lux_power.eps")))
    
    fig.show()
    
def find_power_parameters2(data, args):
    rho = data.power.mean()
    
    sig1 = 0
    sig2 = 0
    
    o = data.index[0]
    e = data.index[-1]
    
    sig1Lock = threading.Lock()
    sig2Lock = threading.Lock()
    
    TList = []
    TListLock = threading.Lock()
    
    data = data.reset_index()
    data.time = data.time - o
    data = data.set_index("time")
    
    data = data.resample("s").mean()
    
    for T in pd.date_range(o, e,freq="s"):
        TList.append((e-T+pd.tseries.offsets.Second()).total_seconds())
        
    lenTList0 = len(TList)
    sig1Q = queue.Queue(lenTList0)
    sig2Q = queue.Queue(lenTList0)
        
    #def fTh(sig1,sig2):
    def fTh(sig1Q,sig2Q):
        while(len(TList)>0):
            TListLock.acquire()
            t = TList.pop()
            TListLock.release()
            
            m = data.power.rolling(window=pd.tseries.offsets.Second(int(t))).sum().max()
            n = rho*t
            s1 = m-n
            s2 = n-m
            sig1Q.put(s1)
            sig2Q.put(s2)
            
            #if s1 > sig1:
            #    sig1Lock.acquire()
            #    sig1 = s1
            #    sig1Lock.release()
            #if s2 > sig2:
            #    sig2Lock.acquire()
            #    sig2 = s2
            #    sig2Lock.release()
                
    threads = []
    nbThreads = 4
    
    for i in range(0,nbThreads-1):
        #threads.append(threading.Thread(target = fTh,args=(sig1,sig2)))
        threads.append(threading.Thread(target = fTh,args=(sig1Q,sig2Q)))
        threads[i].start()

    alive = True
    while(alive):
        alive = False
        while not sig1Q.empty() and not sig2Q.empty():
            s1 = sig1Q.get()
            s2 = sig2Q.get()
            if s1 > sig1:
                sig1 = s1
            if s2 > sig2:
                sig2 = s2
        for t in threads:
            t.join(1)
            if t.is_alive():
                alive = True
        print("Progress: " + str(100*(len(TList)/lenTList0)))
        #print("Temp: $\\rho = " + str(rho) +"$")
        print("Temp: $\sigma_1 = " + str(sig1) +"$")
        print("Temp: $\sigma_2 = " + str(sig2) +"$")
        
    print("$\\rho = " + str(rho) +"$")
    print("$\sigma_1 = " + str(sig1) +"$")
    print("$\sigma_2 = " + str(sig2) +"$")
    
    return (rho,sig1,sig2)
    
def find_power_parameters(data, args):
    
    rho = data.power.mean()
    sig1 = 0
    sig2 = 0
    
    sig1Lock = threading.Lock()
    sig2Lock = threading.Lock()
    
    o = data.index[0]
    e = data.index[-1]
    
    tauList = []
    tauListLock = threading.Lock()
    
    for tau in pd.date_range(o, e,freq="s"):
        tauList.append(tau)
        
    lenTauList0 = len(tauList)
    
    def f(tau,T,rho=rho):
        tau1 = tau
        A = T-tau
        T = A.total_seconds()
        tau2 = tau+A
        x = data.power[tau1:tau2].sum()
        y = rho*T
        return (x-y,y-x)
        
    def f2():
        pass
        
    def fTh(sig1, sig2):
        #global sig1
        #global sig2
        while(len(tauList)>0):
            tauListLock.acquire()
            tau = tauList.pop()
            tauListLock.release()
            
            data.power.rolling(window=tau).sum().max()
            
            for T in pd.date_range(tau+pd.tseries.offsets.Second(), e,freq="s"):
                s1,s2 = f(tau,T)
                sig1Lock.acquire()
                sig2Lock.acquire()
                if s1 > sig1:
                    sig1 = s1
                if s2 > sig2:
                    sig2 = s2
                sig1Lock.release()
                sig2Lock.release()
        
                
    threads = []
    nbThreads = 4
    
    for i in range(0,nbThreads-1):
        threads.append(threading.Thread(target = fTh,args=(sig1,sig2)))
        threads[i].start()

    alive = True
    while(alive):
        alive = False
        for t in threads:
            t.join(1)
            if t.is_alive():
                alive = True
        print("Progress: " + str(100*(len(tauList)/lenTauList0)))
    
                
    print("$\\rho = " + str(rho) +"$")
    print("$\sigma_1 = " + str(sig1) +"$")
    print("$\sigma_2 = " + str(sig2) +"$")
    
    return (rho,sig1,sig2)
        
    
if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description="Generate a report from the data.")
    
    parser.add_argument("path", help=("Path to file"), type=str)
        
    parser.add_argument("-o","--output",help="Output path")
    
    parser.add_argument("-PoT","--powerOverTime",help="Generate a plot of the power over the time.", action="store_true")
    
    parser.add_argument("-f","--fine",help="Generate fine plots.", action="store_true")
    
    parser.add_argument("-cP","--computeParameters",help="Compute rho and sigma.", action="store_true")
    
    parser.add_argument("-eG","--envGraph",help="Generate env graph.", action="store_true")
    
    parser.add_argument("-lpG","--luxPowerGraph",help="Generate lux/power graph.", action="store_true")
    
    main(parser.parse_args())
