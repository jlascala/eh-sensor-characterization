/*
 Energy Harvesting For The Internet-of-Things: Measurements And Probability Models
 
 George Smart <g.smart@ee.ucl.ac.uk>
 John Atkinson <john.atkinson.10@ucl.ac.uk>
 
 Department of Electronic & Electrical Engineering
 University College London
 
 13 July 2015
 
 SD Card Interconnects attach to SPI the bus as follows:
  - MOSI    - pin 11
  - MISO    - pin 12
  - CLK     - pin 13
  - CS      - pin 10

 Analog Sensors Interconnects:
  - Solar   - pin A0
  - Diode   - pin A1
  - Thermal - pin A2
  - Piezo   - pin A3
  
 Envronment Sensors:
  - DHT11   - pin 2
  - BH1750  - I2C Bus (A4/A5)
  
 Debug LEDs:
  - Solar   - pin 4
  - Diode   - pin 5
  - Thermal - pin 6
  - Piezo   - pin 7
*/

#include <SPI.h>
#include <SD.h>
#include <Wire.h>
//#include <BH1750/BH1750.h>
#include <BH1750_WE.h>
#include <DHT.h>

//#include <mbed.h>
//#include <rtos.h>
//#include <mbed_wait_api.h>
//#include <platform/CircularBuffer.h>
//#include <platform/Callback.h>

// Ethernet Shield, CS = pin 4.
// Standard SD Shield, CS = pin 10.
const int chipSelect = 10; 

#define DHTPIN 2
#define DHTTYPE DHT11   // DHT 11 
DHT dht(DHTPIN, DHTTYPE);

#define BH1750_ADDRESS 0x23
BH1750_WE lightMeter(BH1750_ADDRESS);

uint8_t sensor_read = 0;

unsigned long currMeasure = 0;

char filename[] = "LG00.CSV"; // auto set later to be 00-99.

#define VCC 3.3
#define BRIDGE_RESISTOR 1200.0

const float bridgeResistor = BRIDGE_RESISTOR;
const float vcc = VCC;

uint32_t rLoad = 0;

const int ledR = 22;
const int ledG = 23;
const int ledB = 24;

#define DHT    2
#define SDACT  3
#define LED   24
#define BUTTON 3

#define ANALOG_INPUT A0
#define CURRENT_INPUT A1

void measure(uint32_t);
uint32_t measure_resistor(void);
void wait_for_button_press(void);

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  delay(2000);
  /*
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  */
  
  //lightMeter.begin();
  Wire.begin();
  lightMeter.init();
  dht.begin();
  
  Serial.print("Initialising SD card...");
  // set CS to output (uses default for library, even if not used)
  pinMode(10, OUTPUT);
  
  pinMode(SDACT, OUTPUT);
  pinMode(DHT,   INPUT);
  pinMode(LED,  OUTPUT);
  pinMode(BUTTON,  INPUT);
  pinMode(ledR,  OUTPUT);
  pinMode(ledG,  OUTPUT);
  pinMode(ledB,  OUTPUT);

  digitalWrite(ledR,HIGH);
  digitalWrite(ledG,HIGH);
  digitalWrite(ledB,HIGH);

  // see if the card is present and can be initialised:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  Serial.println("Card initialised.");
  
  // find the lowest filename that's not used.
  for (uint8_t i = 0; i < 100; i++) {
   filename[2] = i/10 + '0';
   filename[3] = i%10 + '0';
   if (!SD.exists(filename)) {
    break; 
   }
  }
  
  Serial.print("Logging to: ");
  Serial.println(filename);
  
  digitalWrite(LED, HIGH);
  
  delay(250);
  
  // write CSV column headers
  File dataFile = SD.open(filename, FILE_WRITE);
  dataFile.println("msTime,voltage,current,LightMeter,TempMeter,HumiMeter");
  dataFile.close();

  for(int i = 0;i<10;i++) {
    // Strobe output LEDs for 2sec
    digitalWrite(LED, LOW);
    delay(100);
    digitalWrite(LED, HIGH);
    delay(100);
  }

  analogReadResolution(12);

  //rLoad = measure_resistor();
  delay(500);
  digitalWrite(LED,HIGH);
  digitalWrite(ledR,HIGH);
  wait_for_button_press();
  digitalWrite(ledG,LOW);
  
}

void loop() {
  measure(rLoad);
  //wait_for_button_press();
}

void measure(uint32_t rLoad)
{
  //uint16_t lux = 0;
  float lux = 0.0;
  double humi = 0;
  double temp = 0;
  
  currMeasure = millis();
  // force sensor reading every 300 readings (30 seconds)
  if (sensor_read == 0) {
    //lux = lightMeter.readLightLevel();
    lux = lightMeter.getLux();
    humi = dht.readHumidity();  //DHT11 is very slow
    temp = dht.readTemperature();
  }
  sensor_read++;
  if (sensor_read >= 20) {
    sensor_read = 0;
  }
  
  int voltage = analogRead(ANALOG_INPUT);
  int current = analogRead(CURRENT_INPUT);
  //int Therm = analogRead(A2);
  //int Piezo = analogRead(A3);

  /*
  if (Solar > 0) {digitalWrite(LED, HIGH);} else {digitalWrite(LED, LOW);}
  if (Diode > 0) {digitalWrite(LED, HIGH);} else {digitalWrite(LED, LOW);}
  if (Therm > 0) {digitalWrite(LED, HIGH);} else {digitalWrite(LED, LOW);}
  if (Piezo > 0) {digitalWrite(LED, HIGH);} else {digitalWrite(LED, LOW);}
  */
  
  // make a string for assembling the data to log:
  String dataString = "";
  dataString += String(currMeasure);
  dataString += ",";
  dataString += String(voltage);
  dataString += ",";
  dataString += String(current);
  /*
  dataString += ",";
  dataString += String(Therm);
  dataString += ",";
  dataString += String(Piezo);
  */
  dataString += ",";
  dataString += String(lux);
  dataString += ",";
  dataString += String((int)temp);
  dataString += ",";
  dataString += String((int)humi);
  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  File dataFile = SD.open(filename, FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile) {
    digitalWrite(SDACT, HIGH);
    dataFile.println(dataString);
    dataFile.close();
    digitalWrite(SDACT, LOW);

    Serial.println(dataString);
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.print("error opening ");
    Serial.println(filename);
  }
  
  // wait until the next measurement time...
  while (millis() < (currMeasure + 100));
}

void wait_for_button_press(void) {
  int button = 0;
  uint8_t ledIntensity = 0;
  uint16_t voltage = 0;
  while(!button) {
    button = digitalRead(BUTTON);
    ledIntensity += 16;
    voltage = analogRead(ANALOG_INPUT);
    if (ledIntensity >= 127) {
      digitalWrite(LED,HIGH);
    } else {
      digitalWrite(LED,LOW);
    }
    if (voltage>= 4050) {
      digitalWrite(ledR,LOW);
    } else {
      digitalWrite(ledR,HIGH);
    }
    delay(50);
  }
  digitalWrite(LED,HIGH);
}

uint32_t measure_resistor(void)
{
  uint32_t R = 0;
  uint32_t vMeas = 0;
  uint16_t rLoadData[256];

  uint32_t bR = ((uint32_t)bridgeResistor)<<8;
  uint32_t vccInt = (uint32_t)(vcc*4096);

  uint32_t a = 0;
  
  Serial.println("Connect the bridge resistor and press the button.");
  wait_for_button_press();
  
  Serial.println("OK ! Measuring resistor");

  for(uint16_t i = 0;i<=255;i++) {
    digitalWrite(ledR,HIGH);
    rLoadData[i] = analogRead(ANALOG_INPUT);
    //Serial.print(".");
    delay(20);
    digitalWrite(ledR,LOW);
  }

  Serial.println(" Done !");

  for(uint16_t i = 0;i<=255;i++) {
    vMeas += rLoadData[i];
  }

  Serial.print("Measured voltage = ");
  Serial.print((float)(vMeas>>8)/(pow(2,10)));
  Serial.println(" [V].");

  a = vMeas/vccInt;
  R = a*bR*(1/((1<<8)*a))>>8; // Pose probablement problème à cause de la division

  Serial.print("The load resistance Rl = ");
  Serial.print(rLoad);
  Serial.println(" Ohm.");

  digitalWrite(LED,HIGH);
  digitalWrite(ledR,HIGH);
  delay(1000);

  return R;
}
