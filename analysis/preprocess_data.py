import pandas as pd
import numpy as np
from pathlib import Path

def main(args):
    print(args.path)
    data = pd.read_csv(args.path)
    #print(data.columns)
    
    output = None
    p = Path(args.path)
    if args.output is None:
        output = p.resolve().parent
        output = output / (p.stem + "_preprocessed.pkl")
    else:
        output = args.output

    if output.exists() and output.is_dir()==False:
        output = output.parent
        output = output / (p.stem + "_preprocessed.pkl")
        
    if args.type == "testbed_m1geo":
        preprocess_testbed_m1geo(data,output)
    elif args.type == "IV1":
        preprocess_IV1(data,output,args)
    elif args.type == "OSC":
        preprocess_osc(data,output,args)

def preprocess_IV1(data,output,args):
    currentExponent = -3
    if args.microAmpere:
        currentExponent -6
    elif args.nanoAmpere:
        currentExponent = -9
    
    nbBitAdc = 12
    voltageColumns = ["voltage"]
    
    data = data.rename(columns={"msTime":"time"})
    
    o = None
    if args.startTime is None:
        o = pd.to_datetime("03/06/2020 12:00", dayfirst=True)
    else:
        o = pd.to_datetime(args.startTime, dayfirst=True)
    data.time = pd.to_datetime(data.time, unit="ms", origin = o)
    
    data = data.set_index("time")
    for c in voltageColumns:
        data[c] = data[c]/(2**nbBitAdc)
        
    data["current"] = (10**currentExponent)*data["current"]/(2**nbBitAdc)
    
    data.insert(3,"power", data["voltage"]*data["current"])
    
    print("Output to " + str(output))
    data.to_pickle(str(output))
    print(data)
    
def preprocess_osc(data,output,args):
    currentExponent = -3
    if args.microAmpere:
        currentExponent -6
    elif args.nanoAmpere:
        currentExponent = -9
    
    nbBitAdc = 12
    voltageColumns = ["voltage"]
    
    data = pd.read_csv(args.path, converters={"x-axis":np.float32,"2":np.float32,"3":np.float32}, skiprows=[1])\
            .rename(columns={"x-axis":"time","1":"voltage","2":"current"})
    
    o = None
    if args.startTime is None:
        o = pd.to_datetime("03/06/2020 12:00", dayfirst=True)
    else:
        o = pd.to_datetime(args.startTime, dayfirst=True)
    data.time = pd.to_datetime(data.time, unit="s", origin = o)
    
    data = data.set_index("time")
        
    data["current"] = (10**currentExponent)*data["current"]
    
    data.insert(2,"power", data["voltage"]*data["current"])
    
    print("Output to " + str(output))
    data.to_pickle(str(output))
    print(data)
    
def preprocess_testbed_m1geo(data, output):
    nbBitAdc = 10
    voltageColumns = ["Solar","Therm","Diode","Piezo"]
    
    output = Path(output).resolve()
    if output.exists() and output.is_dir()==False:
        output = output.parent
    
    data = data.set_index("msTime")
    for c in voltageColumns:
        data[c] = data[c]/(2**nbBitAdc)
    print(data)
    
    
    

if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description="Preprocess the data.")
    
    parser.add_argument("path", help=("Path to file"), type=str)
    
    parser.add_argument("-t","--type", help="Type of data",type=str)
    
    parser.add_argument("-st","--startTime", help="Starting time of the experiment in ",type=str)
    
    parser.add_argument("-o","--output",help="Output path")
    
    parser.add_argument("-mA","--milliAmpere",help="Indicates that the current sensor was set to measure in the milli amp range.", action="store_true")
    parser.add_argument("-uA","--microAmpere",help="Indicates that the current sensor was set to measure in the micro amp range.", action="store_true")
    parser.add_argument("-nA","--nanoAmpere",help="Indicates that the current sensor was set to measure in the nano amp range.", action="store_true")
    
    main(parser.parse_args())
    
